package com.crosstech.dbwrapper;

import java.sql.*;

/**
 * Простой клиент для тестирования
 */
public class Client {

    private static final String HOST = "localhost";
    private static final int PORT = 5454;
    private static final String DATABASE = "tdmdb";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "postgres";
    private static final String QUERY = "SELECT * FROM public.tdm_db_data_type";// имя пользователя БД

    public static void send() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try (Connection con = DriverManager.getConnection("jdbc:postgresql://" + HOST + ":" + PORT + '/' + DATABASE, USERNAME, PASSWORD)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(QUERY);
            System.out.println(rs.toString());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        send();
    }
}
