package com.crosstech.dbwrapper.enums;

public enum DbType {

    ORACLE("Oracle"),
    MSSQL("MSSQL"),
    MYSQL("MySQL"),
    POSTGRESQL("PostgreSQL"),
    CLICK_HOUSE("ClickHouse"),
    APACHE_HIVE("ApacheHive"),
    SYBASE("Sybase"),
    VERTICA("Vertica");

    private final String id;

    DbType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public static DbType fromId(String id) {
        for (DbType dbType : DbType.values()) {
            if (dbType.getId().equals(id)) {
                return dbType;
            }
        }
        return null;
    }
}
