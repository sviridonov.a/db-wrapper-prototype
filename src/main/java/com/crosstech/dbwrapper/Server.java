package com.crosstech.dbwrapper;

import com.crosstech.dbwrapper.enums.DbType;
import com.crosstech.dbwrapper.handler.DbHandler;
import com.crosstech.dbwrapper.handler.DbHandlerManager;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Server {
    private static final boolean MASKING_ENABLED = false;

    private static final int DEFAULT_PORT = 5454;
    private static final String DB_HOST = "localhost";
    private static final int DB_PORT = 5432;
    private static final DbType DATABASE_TYPE = DbType.POSTGRESQL;

    private ServerSocket serverSocket;

    /**
     * Start server on default port
     * @throws IOException
     */
    public void start() throws IOException {
        start(DEFAULT_PORT);
    }

    /**
     * Start server on specified port
     * @param port server port number
     * @throws IOException
     */
    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        System.out.println("Server is started on port " + port);
        System.out.println("Waiting for incoming connections...");

        while (true) {
            // при входящем поключении создаём сокет для работы с приложением в отдельном потоке
            new Server.ClientHandler(serverSocket.accept(), DATABASE_TYPE, DB_HOST, DB_PORT).start();
        }
    }

    public void stop() throws IOException {
        if (serverSocket != null) { serverSocket.close(); }
    }


    class ClientHandler extends Thread {

        private final Socket appClientSocket; // сокет для связи с клиентским приложением
        private final DbHandler dbHandler;
        private final String dbHost;
        private final int dbPort;

        // потоки данных между сервером и клиентским приложением
        private InputStream appInputStream;
        private OutputStream appOutputStream;

        // потоки данных между сервером и БД
        private InputStream dbInputStream;
        private OutputStream dbOutputStream;


        public ClientHandler(Socket socket, DbType dbType, String dbHost, int dbPort) {
            appClientSocket = socket;
            this.dbHost = dbHost;
            this.dbPort = dbPort;
            this.dbHandler = DbHandlerManager.getHandler(dbType);
            System.out.println("Connected " + socket.getInetAddress().toString() + ':' + socket.getPort());
        }

        @Override
        public void run() {
            try {
                // создаём соединение с БД
                Socket dbClientSocket = new Socket(dbHost, dbPort);
                dbInputStream = new BufferedInputStream(dbClientSocket.getInputStream());
                dbOutputStream = new BufferedOutputStream(dbClientSocket.getOutputStream());
                System.out.println("Connected to database " + dbHost + ':' + dbPort);

                appInputStream = new BufferedInputStream(appClientSocket.getInputStream());
                appOutputStream = new BufferedOutputStream(appClientSocket.getOutputStream());

                // ----------------------------
                // ОЖИДАЕМ ДАННЫЕ ОТ ПРИЛОЖЕНИЯ
                // ----------------------------
                boolean hasData = false;
                while (true) {
                    //System.out.print("Output:");
                    StringBuilder appInput = new StringBuilder();
                    StringBuilder appInputNum = new StringBuilder();
                    while (appInputStream.available() > 0) {
                        hasData = true;
                        // читаем запрос из приложения
                        int read = appInputStream.read();
                        appInput.append((char) read);
                        appInputNum.append(Integer.valueOf(read)).append(' ');
                        // записываем данные для дальнейшей отправки в БД
                        dbOutputStream.write(read);
                    }

                    if (hasData) {
                        // определяем, SELECT-запрос или нет
                        boolean isSelectQuery = dbHandler.detectSelectQuery(appInput.toString());

                        System.out.println("App > DB" + (isSelectQuery ? " [SELECT QUERY]" : "") + ": " + appInput);
                        //System.out.println("App > DB (int): " + appInput);

                        // перенаправляем запрос в БД
                        dbOutputStream.flush();
                        System.out.println("Sent to database\n");
                        hasData = false;

                        ByteBuffer byteBuffer = null;

                        // -----------------------------
                        // ОЖИДАЕМ ОТВЕТА ОТ БАЗЫ ДАННЫХ
                        // -----------------------------
                        boolean hasDbData = false;
                        while (true) {
                            StringBuilder dbInput = new StringBuilder();
                            StringBuilder dbInputNum = new StringBuilder();
                            int b = 0;
                            while (dbInputStream.available() > 0) {
                                if (isSelectQuery && byteBuffer == null) {
                                    byteBuffer = ByteBuffer.allocate(dbInputStream.available());
                                }
                                hasDbData = true;
                                // читаем данные из БД
                                int dbRead = dbInputStream.read();
                                dbInput.append((char) dbRead);
                                dbInputNum.append(Integer.valueOf(dbRead)).append(' ');
                                if (MASKING_ENABLED && isSelectQuery) {
                                    byteBuffer.put((byte) dbRead);
                                } else {
                                    // записываем данные для дальнейшей отправки в приложение
                                    appOutputStream.write(dbRead);
                                }
                            }
                            if (hasDbData) {
                                System.out.println("DB > App" + (isSelectQuery ? " [QUERY RESULT]" : "") + ": " + dbInput);
                                //System.out.println("DB > App (int): " + dbInputNum);
                                if (MASKING_ENABLED && isSelectQuery) {
                                    // TODO 1. Парсинг результатов запроса (разбивка по строкам, извлечение имён столбцов и данных
                                    // TODO 2. Маскирование необходимых полей
                                    // TODO 3. Упаковка маскированных данных в ResultSet и отправка в приложение
                                } else {
                                    // перенаправляем ответ БД в приложение
                                    appOutputStream.flush();
                                }
                                System.out.println("Sent to client\n");
                                break;
                            }
                        }
                    }
                }
            } catch (IOException ex) {
                System.err.println("ERROR: " + ex.getLocalizedMessage());
            } finally {
                try {
                    if (appInputStream != null) { appInputStream.close(); }
                    if (appOutputStream != null) { appOutputStream.close(); }
                    if (dbInputStream != null) { dbInputStream.close(); }
                    if (dbOutputStream != null) { dbOutputStream.close(); }
                } catch (IOException ex) {
                    System.err.println("ERROR: " + ex.getLocalizedMessage());
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            new Server().start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
