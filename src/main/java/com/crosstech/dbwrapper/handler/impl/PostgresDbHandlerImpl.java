package com.crosstech.dbwrapper.handler.impl;

import com.crosstech.dbwrapper.handler.DbHandler;

public class PostgresDbHandlerImpl implements DbHandler {

    @Override
    public boolean detectSelectQuery(String request) {
        boolean is = request.startsWith("P") && request.length() >= 12
                && "SELECT".equalsIgnoreCase(request.substring(6, 12));
        return is;
    }
}
