package com.crosstech.dbwrapper.handler;

public interface DbHandler {

    /**
     * Определяет, является ли указанный запрос SELECT-запросом
     * @param request запрос
     */
    boolean detectSelectQuery(String request);
}
