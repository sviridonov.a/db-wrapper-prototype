package com.crosstech.dbwrapper.handler;

import com.crosstech.dbwrapper.enums.DbType;
import com.crosstech.dbwrapper.exception.DbHandlerNotFoundException;
import com.crosstech.dbwrapper.handler.impl.PostgresDbHandlerImpl;

import java.util.Map;

/** Класс для получения реализации обработчика запросов в зависимости от типа БД */
public class DbHandlerManager {

    private static final Map<DbType, DbHandler> HANDLERS = Map.of(
            DbType.POSTGRESQL, new PostgresDbHandlerImpl()
    );

    public static DbHandler getHandler(DbType dbType) {
        if (!HANDLERS.containsKey(dbType)) {
            throw new DbHandlerNotFoundException();
        }
        return HANDLERS.get(dbType);
    }

    private DbHandlerManager() { }
}
